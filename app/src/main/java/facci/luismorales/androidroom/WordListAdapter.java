package facci.luismorales.androidroom;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

public class WordListAdapter extends ListAdapter<facci.luismorales.androidroom.Word, facci.luismorales.androidroom.WordViewHolder> {

    public WordListAdapter(@NonNull DiffUtil.ItemCallback<facci.luismorales.androidroom.Word> diffCallback) {
        super(diffCallback);
    }

    @Override
    public facci.luismorales.androidroom.WordViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return facci.luismorales.androidroom.WordViewHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(facci.luismorales.androidroom.WordViewHolder holder, int position) {
        facci.luismorales.androidroom.Word current = getItem(position);
        holder.bind(current.getWord());
    }

    static class WordDiff extends DiffUtil.ItemCallback<facci.luismorales.androidroom.Word> {

        @Override
        public boolean areItemsTheSame(@NonNull facci.luismorales.androidroom.Word oldItem, @NonNull facci.luismorales.androidroom.Word newItem) {
            return oldItem == newItem;
        }

        @Override
        public boolean areContentsTheSame(@NonNull facci.luismorales.androidroom.Word oldItem, @NonNull facci.luismorales.androidroom.Word newItem) {
            return oldItem.getWord().equals(newItem.getWord());
        }
    }
}
